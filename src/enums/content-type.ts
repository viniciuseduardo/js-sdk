export enum XanoContentType {
    JSON = 'application/json',
    Multipart = 'multipart/form-data',
    Text = 'text/plain',
};
