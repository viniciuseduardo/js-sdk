export interface XanoFormData {
    formData: any;
    hasFile: boolean;
    rawFormData: Record<any, any>;
}
