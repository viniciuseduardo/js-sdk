import { XanoBaseClient } from './base-client';
export declare class XanoNodeClient extends XanoBaseClient {
    protected getFormDataInstance(): any;
    protected appendFormData(formData: any, key: string, value: any): void;
}
//# sourceMappingURL=node-client.d.ts.map