"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XanoContentType = void 0;
var XanoContentType;
(function (XanoContentType) {
    XanoContentType["JSON"] = "application/json";
    XanoContentType["Multipart"] = "multipart/form-data";
    XanoContentType["Text"] = "text/plain";
})(XanoContentType = exports.XanoContentType || (exports.XanoContentType = {}));
;
//# sourceMappingURL=content-type.js.map