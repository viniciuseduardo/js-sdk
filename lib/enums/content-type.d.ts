export declare enum XanoContentType {
    JSON = "application/json",
    Multipart = "multipart/form-data",
    Text = "text/plain"
}
//# sourceMappingURL=content-type.d.ts.map